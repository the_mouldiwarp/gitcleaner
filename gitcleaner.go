package main

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

// splitCommandString takes the command line as a string and breaks it into an array.
func splitCommandString(s string) []string {
	r := strings.Split(s, " ")
	return r
}

// SplitComArgs Takes a command string (wit and arguments) and returns a string
// representing the executable and an array representing the arguments.
func splitComArgs(s string) (string, []string) {
	i := splitCommandString(s)
	return i[0], i[1:]
}

// outToStr takes a byte array and converts it to a string
func outToStr(b []byte) string {
	return string(b[:len(b)])
}

// strSplitNewline takes a string and creates a string slice, splitting on newline
func strSplitNewLine(s string) []string {
	return strings.Split(s, "\n")
}

// Split a string into a slice using / as the delimiter
func strSplitOnForwardslash(s string) []string {
	return strings.Split(s, "/")
}

// ExecuteCmd takes a command and arguments as a string and returns a slice of data
// representing the command output
func ExecuteCmd(s string) []string {
	c, a := splitComArgs(s)

	var (
		co []byte
		e  error
	)
	if co, e = exec.Command(c, a...).Output(); e != nil {
		fmt.Fprintln(os.Stderr, "There was an error running the command: ", e)
		os.Exit(1)
	}
	return strSplitNewLine(outToStr(co))
}

func getpwd() string {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return pwd
}

func isreporoot(path string) bool {
	if _, err := os.Stat(fmt.Sprintf("%s/.git", path)); os.IsNotExist(err) {
		return false
	}
	return true
}

func masterbranch() string {
	if len(os.Args) > 1 {
		return os.Args[1]
	}

	return "master"
}

func filterlist(list []string) []string {
	var result []string
	for _, b := range list {
		match, _ := regexp.MatchString("origin/master|origin/release", b)
		if match == true {
			continue
		} else {
			if b == "" {
				continue
			}
			result = append(result, b)
		}
	}
	return result
}

func joinarraywithfslash(p []string) string {
	return strings.Join(p, "/")
}

func main() {
	path := getpwd()
	mb := masterbranch()

	if isreporoot(path) == false {
		fmt.Println(fmt.Sprintf("%s Is not a repository or you are not in the root", path))
	} else {
		mergedbranches := ExecuteCmd(fmt.Sprintf("git branch -r --merged %s", mb))
		for _, v := range filterlist(mergedbranches) {
			b := joinarraywithfslash(strSplitOnForwardslash(v)[1:])
			fmt.Printf("git push origin :%s\n", b)
		}
	}
}
